<?php

namespace Fricfrac\Controllers;

class EventCategoryController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }
    public function readingOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventCategory', $id);
        //var_dump($model['row']);
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'EventCategory',
            'error' => 'Geen'
        );
        $eventCategory = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('EventCategory', $eventCategory, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$eventCategory['Name']} is toegevoegd aan EventCategory";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventCategory['Name']} niet toevoegen aan EventCategory";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        return $this->view($model, 'Views/EventCategory/Index.php');
    }
    //Deze is om de view juist te zien --> dus dat de gegevens zijn ingevuld
    public function updatingOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventCategory', $id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }
    public function updateOne()
    {
        $model = array(
            'tableName' => 'EventCategory',
            'error' => 'Geen'
        );
        $eventCategory = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::update('EventCategory', $eventCategory, 'Name')) {
            $model['message'] = "Rij aangepast! {$eventCategory['Name']} is aangepast in EventCategory";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventCategory['Name']} niet aanpassen in EventCategory";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        return $this->view($model, 'Views/EventCategory/Index.php');
    }
    public function deleteOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventCategory', $id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        if (\AnOrmApart\Dal::delete('EventCategory', $id)){
            $model['message'] = "Rij verwijdert!";
        } else{
            $model['message'] = "Rij niet verwijdert!";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventCategory');
        return $this->view($model, 'Views/EventCategory/Index.php');
    }

}
