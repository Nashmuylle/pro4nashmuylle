<section class="show-room entity">
    <form id="form" method="post" action="/Event/updateOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <span class="icon-floppy-disk"></span>
                <span class="screen-reader-text">Update One</span>
            </button>
            <a href="/Event/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <input type="hidden" id="Id" name="Id" value=<?php echo $model['row']['id']; ?>/>
            <div>
                <label for="Events-Name">Naam</label>
                <input id="Events-Name" name="Events-Name" type="text"
                    value="<?php echo $model['row']['name'];?>" required />
            </div>
            <div>
                <label for="EventsLocation">Locatie</label>
                <input id="EventsLocation" name="EventsLocation" type="text"
                    value="<?php echo $model['row']['location'];?>" required />
            </div>
            <div>
                <label for="EventsStarts">Start</label>
                <input id="EventsStarts" name="EventsStarts" type="datetime-local"
                    value="<?php echo $model['row']['starts'];?>" />
            </div>
            <div>
                <label for="EventsEnds">Einde</label>
                <input id="EventsEnds" name="EventsEnds" type="datetime-local"
                    value="<?php echo $model['row']['ends'];?>" />
            </div>
            <div>
                <label for="EventsImage">Afbeelding</label>
                <input id="EventsImage" name="EventsImage" type="text"
                    value="<?php echo $model['row']['image'];?>" required />
            </div>
            <div>
                <label for="EventsDescription">Beschrijving</label>
                <input id="EventsDescription" name="EventsDescription" type="text"
                    value="<?php echo $model['row']['description'];?>" required />
            </div>
            <div>
                <label for="EventsOrganiserName">Organizator naam</label>
                <input id="EventsOrganiserName" name="EventsOrganiserName" type="text"
                    value="<?php echo $model['row']['organiserName'];?>" required />
            </div>
            <div>
                <label for="EventsOrganiserDescription">Organizator beschrijving</label>
                <input id="EventsOrganiserDescription" name="EventsOrganiserDescription" type="text"
                    value="<?php echo $model['row']['organiserDescription'];?>" required />
            </div>
            <div>
                <label for="Events-EventCategory">Event Category</label>
                <select id="Events-EventCategory" name="Events-EventCategory">
                <?php
                foreach ($model['categoryOptions'] as $eventCategory) {
                    ?>
                    <option value="<?php echo $eventTopic['id'] ?>"
                    <?php echo $model['row']['eventCategoryID'] === $eventCategory['id'] ? 'selected = "selected"' : '';?>>
                    <?php echo $eventCategory['name'] ?></option>
                <?php } ?>
                </select>
            </div>
            <div>
                <label for="Events-EventTopicId">Event Topic</label>
                <select id="Events-EventTopicId" name="Events-EventTopicId">
                <?php
                foreach ($model['topicOptions'] as $eventTopic) {
                ?>
                    <option value="<?php echo $eventTopic['id'] ?>"
                    <?php echo $model['row']['eventTopicID'] === $eventTopic['id'] ? 'selected = "selected"' : '';?>>
                    <?php echo $eventTopic['name'] ?></option>
                <?php } ?>
                </select>
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>