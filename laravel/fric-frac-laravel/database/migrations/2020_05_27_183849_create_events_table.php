<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('location');
            $table->dateTime('starts');
            $table->dateTime('ends');
            $table->string('description');
            $table->string('organiserName');
            $table->string('organiserDescription');
            $table->integer('eventcategoryid')->unsigned();
            $table->foreign('eventcategoryid')->references('id')->on('event_categories');
            $table->integer('eventtopicid')->unsigned();
            $table->foreign('eventtopicid')->references('id')->on('event_topics');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('events');

    }
}
