@extends('layout')
@section('content')
    <h1>Fric Frac Laravel</h1>
    <article class="index">
        <a class="tile" href="{{ route('eventcategory.index') }}">
        <span aria-hidden="true" class="icon-bookmark"></span>
        <span class="screen-reader-text">Event Category</span>
        Event Category
        </a>
        <a class="tile" href="{{ route('eventtopic.index') }}">
            <span aria-hidden="true" class="icon-price-tag"></span>
            <span class="screen-reader-text">Event Topic</span>
            Event Topic</a>
        <a class="tile" href="{{ route('event.index') }}">
            <span aria-hidden="true" class="icon-power"></span>
            <span class="screen-reader-text">Event</span>
            Event Index</a>
        <div class="tile _2x1">Informatieve tegel</div>
    </article>
@endsection
