
<!DOCTYPE html>
<html>
<head>
    <title>Laravel 7 CRUD Application - ItSolutionStuff.com</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/app.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/css/icon-font.css') }}" />
</head>

<body class="page">
<header class="page-header">
    <nav class="control-panel">
        <a href="/" class="tile">
            <span class="icon-menu"></span>
            <span class="screen-reader-text">Admin index</span>
        </a>
    </nav>
    <h1 class="banner">Fric-frac</h1>
</header>
    @yield('content')

<footer class="page-footer">
    <p>&copy ModernWays 2020</p>
    <p>Opdracht Programmeren 4</p>
</footer>
</body>
</html>
