@extends('layout')
@section('content')


    <section class="show-room entity">
        <form id="form" method="post" action="{{route('eventtopic.store')}}" class="detail">
            @csrf
            <nav class="command-panel">
                <h2 class="banner">EventTopic</h2>
                <button type="submit" value="insert" name="uc" class='tile'>
                    <span class="icon-floppy-disk"></span>
                    <span class="screen-reader-text">Insert One</span>
                </button>
                <a href="{{ route('eventtopic.index') }}" class="tile">
                    <span class="icon-cross"></span>
                    <span class="screen-reader-text">Annuleren</span>
                </a>
            </nav>
            <fieldset>
                <div>
                    <label for="name">Naam</label>
                    <input type="text" required id="name" name="name" />
                </div>
            </fieldset>
            @if ($errors->any())
                <div class="feedback">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
        </form>
        <aside class="list">
            <table>
                <tr>
                    <th></th>
                    <th>Naam</th>
                </tr>
                @foreach ($eventTopics as $item)
                    <tr>
                        <td>
                            <a href="{{ route('eventtopic.show',$item->id)}}">
                                <span class="icon-arrow-right"></span>
                                <span class="screen-reader-text">ReadingOne</span></a>
                        </td>
                        <td>{{$item->name }}</td>

                    </tr>
                @endforeach
            </table>
        </aside>
    </section>






@endsection
