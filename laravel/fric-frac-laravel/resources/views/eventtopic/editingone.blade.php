@extends('layout')
@section('content')




    <section class="show-room entity">
        <form id="form" method="post" action="{{ route('eventtopic.update',$eventtopic_info->id) }}" class="detail">
            @csrf
            @method('PATCH')
            <nav class="command-panel">
                <h2 class="banner">EventTopic</h2>
                <button type="submit" value="insert" name="uc" class='tile'>
                    <span class="icon-floppy-disk"></span>
                    <span class="screen-reader-text">Update One</span>
                </button>
                <a href="{{ route('eventtopic.index') }}" class="tile">
                    <span class="icon-cross"></span>
                    <span class="screen-reader-text">Annuleren</span>
                </a>
            </nav>
            <fieldset>
                <input type="hidden" id="Id" name="Id" value="{{$eventtopic_info->id}}" />
                <div>
                    <label for="name">Naam</label>
                    <input id="name" name="name" type="text" value={{$eventtopic_info->name}} required />
                </div>
            </fieldset>
            <div class="feedback">
            </div>
        </form>
        <aside class="list">
            <table>
                <tr>
                    <th></th>
                    <th>Naam</th>
                </tr>
                @foreach ($eventTopics as $item)
                    <tr>
                        <td>
                            <a href="{{ route('eventtopic.show',$item->id)}}">
                                <span class="icon-arrow-right"></span>
                                <span class="screen-reader-text">ReadingOne</span></a>
                        </td>
                        <td>{{$item->name }}</td>

                    </tr>
                @endforeach
            </table>
        </aside>
    </section>



    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

@endsection
