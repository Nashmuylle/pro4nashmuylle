@extends('layout')
@section('content')




    <section class="show-room entity">
        <form id="form" method="post" action="{{ route('event.update',$event_info->id) }}" class="detail">
            @csrf
            @method('PATCH')
            <nav class="command-panel">
                <h2 class="banner">Event</h2>
                <button type="submit" value="insert" name="uc" class='tile'>
                    <span class="icon-floppy-disk"></span>
                    <span class="screen-reader-text">Update One</span>
                </button>
                <a href="{{ route('event.index') }}" class="tile">
                    <span class="icon-cross"></span>
                    <span class="screen-reader-text">Annuleren</span>
                </a>
            </nav>
            <fieldset>
                <div>
                    <label for="name">Naam</label>
                    <input type="text" required id="name" value="{{$event_info->name}}" name="name" />
                    <div>
                        <!-- HIER MOET ER EEN KEUZE KUNNEN WORDEN GEMAAKT-->
                        <label for="eventtopicid">EventTopic</label>
                        <select name="eventtopicid" id="eventtopicid">
                            @foreach ($eventTopics as $item)
                                @if($item->id == $event_info->eventtopicid)
                                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                @else
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <!-- HIER MOET ER EEN KEUZE KUNNEN WORDEN GEMAAKT-->
                        <label for="eventcategoryid">EventCategory</label>
                        <select name="eventcategoryid" id="eventcategoryid">
                            @foreach ($eventCategories as $item)
                                @if($item->id == $event_info->eventcategoryid)
                                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                @else
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif

                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label for="description">EventsDescription</label>
                        <input id="description" name="description" type="text" value="{{$event_info->description}}" />
                    </div>
                    <div>
                        <label for="location">eventLocation</label>
                        <input id="location" name="location" type="text" value="{{$event_info->location}}" />
                    </div>
                    <div>
                        <label for="organiserDescription">OrganiserDescription</label>
                        <input id="organiserDescription" name="organiserDescription" type="text" value="{{$event_info->organiserDescription}}" />
                    </div>
                    <div>
                        <label for="organiserName">OrganiserName</label>
                        <input id="organiserName" name="organiserName" type="text" value="{{$event_info->organiserName}}" />
                    </div>
                    <div>
                        <label for="starts">Starts</label>
                        <input id="starts" name="starts" type="date" value="{{$event_info->starts}}" />
                    </div>
                    <div>
                        <label for="ends">Ends</label>
                        <input id="ends" name="ends" type="date" value="{{$event_info->ends}}" />
                    </div>
                </div>
            </fieldset>
            <div class="feedback">
            </div>
        </form>
        <aside class="list">
            <table>
                <tr>
                    <th></th>
                    <th>Naam</th>
                </tr>
                @foreach ($events as $item)
                    <tr>
                        <td>
                            <a href="{{ route('event.show',$item->id)}}">
                                <span class="icon-arrow-right"></span>
                                <span class="screen-reader-text">ReadingOne</span></a>
                        </td>
                        <td>{{$item->name }}</td>

                    </tr>
                @endforeach
            </table>
        </aside>
    </section>



    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

@endsection
