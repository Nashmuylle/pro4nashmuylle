@extends('layout')
@section('content')
    <section class="show-room entity">
        <div class="detail">
            <nav class="command-panel">
                <h2 class="banner">Event</h2>
                <a href="{{ route('event.create') }}" class="tile">
                    <span class="icon-plus"></span>
                    <span class="screen-reader-text">InsertingOne</span>
                </a>
            </nav>
            <fieldset></fieldset>
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </div>
        <aside class="list">
            <table>
                <tr>
                    <th></th>
                    <th>Naam</th>
                </tr>
                @foreach ($events as $item)
                <tr>
                    <td>
                        <a href="{{ route('event.show',$item->id)}}">
                            <span class="icon-arrow-right"></span>
                            <span class="screen-reader-text">ReadingOne</span></a>
                    </td>
                    <td>{{$item->name }}</td>

                </tr>
                @endforeach
            </table>
        </aside>
    </section>
@endsection



























