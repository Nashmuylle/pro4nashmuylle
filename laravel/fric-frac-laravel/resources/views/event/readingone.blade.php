@extends('layout')
@section('content')

    <section class="show-room entity">
        <form id="form" method="post" action="{{ route('event.destroy', $event_info->id)}}" class="detail">
            @csrf
            @method('DELETE')
            <nav class="command-panel">
                <h2 class="banner">Event</h2>
                <a href="{{ route('event.edit',$event_info->id)}}" class="tile">
                    <span class="icon-pencil"></span>
                    <span class="screen-reader-text">Updating One</span>
                </a>
                <a href="{{ route('event.create') }}" class="tile">
                    <span class="icon-plus"></span>
                    <span class="screen-reader-text">Inserting One</span>
                </a>
                <button href="{{ route('event.destroy',$event_info->id)}}" class="tile">
                    <span class="icon-bin"></span>
                    <span class="screen-reader-text">Delete One</span>
                </button>
                <a href="{{ route('event.index') }}" class="tile">
                    <span class="icon-cross"></span>
                    <span class="screen-reader-text">Annuleren</span>
                </a>
            </nav>
            <fieldset>
                <div>
                    <label for="Name">Naam:</label>
                    <span>{{ $event_info->name }}</span>
                </div>
            </fieldset>
            <div class="feedback"></div>
        </form>
        <aside class="list">
            <table>
                <tr>
                    <th></th>
                    <th>Naam</th>
                </tr>
                @foreach ($events as $item)
                    <tr>
                        <td>
                            <a href="{{ route('event.show',$item->id)}}">
                                <span class="icon-arrow-right"></span>
                                <span class="screen-reader-text">ReadingOne</span></a>
                        </td>
                        <td>{{$item->name }}</td>

                    </tr>
                @endforeach
            </table>
        </aside>
    </section>


@endsection
