<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name',
        'location',
        'starts',
        'ends',
        'description',
        'organiserName',
        'organiserDescription',
        'eventcategoryid',
        'eventtopicid',
    ];
}
