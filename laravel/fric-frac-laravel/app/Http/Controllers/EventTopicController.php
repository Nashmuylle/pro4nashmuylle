<?php
/*
https://www.positronx.io/php-laravel-crud-operations-mysql-tutorial/
https://www.itsolutionstuff.com/post/laravel-7-crud-example-laravel-7-tutorial-for-beginnersexample.html
*/
namespace App\Http\Controllers;

use App\EventCategory;
use App\EventTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventtopics = EventTopic::all();
        return view('eventtopic.index', compact('eventtopics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['eventTopics'] = EventTopic::all();
        return view('eventtopic.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventTopic::create($request->all());
        return redirect()->route('eventtopic.index')

            ->with('success', 'Eventtopic created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EventTopic $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['eventTopics'] = EventTopic::all();
        $where = array('id' => $id);
        $data['eventtopic_info'] = EventTopic::where($where)->first();
        return view('eventtopic.readingone')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['eventTopics'] = EventTopic::all();
        $where = array('id' => $id);
        $data['eventtopic_info'] = EventTopic::where($where)->first();
        return view('eventtopic.editingone')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
        ]);
        $category = EventTopic::find($id);
        $category->name = $request->get('name');
        $category->save();
        $eventtopics = EventTopic::all();
        return view("eventtopic.index",compact('eventtopics'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = EventTopic::find($id);
        $item -> delete();
        $eventtopics = EventTopic::all();

        return view("eventtopic.index",compact('eventtopics'));



    }
}
