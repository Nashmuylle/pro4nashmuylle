<?php
/*
https://www.positronx.io/php-laravel-crud-operations-mysql-tutorial/
https://www.itsolutionstuff.com/post/laravel-7-crud-example-laravel-7-tutorial-for-beginnersexample.html
*/
namespace App\Http\Controllers;

use App\Event;
use App\EventCategory;
use App\EventTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['events'] = Event::all();
        $data['eventCategories'] = EventCategory::all();
        $data['eventTopics'] = EventTopic::all();
        return view('event.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'starts'=> 'required',
            'ends'=> 'required',
            'description'=> 'required',
            'organiserName'=> 'required',
            'organiserDescription'=> 'required',
            'eventcategoryid'=> 'required',
            'eventtopicid'=> 'required'
        ]);
        Event::create($request->all());
        return redirect()->route('event.index')

            ->with('success', 'Event created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Event $events
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['events'] = Event::all();
        $where = array('id' => $id);
        $data['event_info'] = Event::where($where)->first();
        return view('event.readingone')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $events
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['events'] = Event::all();
        $where = array('id' => $id);
        $data['event_info'] = Event::where($where)->first();
        $data['eventCategories'] = EventCategory::all();
        $data['eventTopics'] = EventTopic::all();
        return view('event.editingone')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $events
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'starts'=> 'required',
            'ends'=> 'required',
            'description'=> 'required',
            'organiserName'=> 'required',
            'organiserDescription'=> 'required',
            'eventcategoryid'=> 'required',
            'eventtopicid'=> 'required'
        ]);
        $category = Event::find($id);
        $category->name = $request->get('name');
        $category->location = $request->get('location');
        $category->starts = $request->get('starts');
        $category->ends = $request->get('ends');
        $category->description = $request->get('description');
        $category->organiserName = $request->get('organiserName');
        $category->organiserDescription = $request->get('organiserDescription');
        $category->eventcategoryid = $request->get('eventcategoryid');
        $category->eventtopicid = $request->get('eventtopicid');
        $category->save();
        $events = Event::all();
        return view("event.index",compact('events'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $events
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Event::find($id);
        $item -> delete();
        $events = Event::all();

        return view("event.index",compact('events'));



    }
}
