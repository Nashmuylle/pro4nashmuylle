<?php
/*
https://www.positronx.io/php-laravel-crud-operations-mysql-tutorial/
https://www.itsolutionstuff.com/post/laravel-7-crud-example-laravel-7-tutorial-for-beginnersexample.html
*/
namespace App\Http\Controllers;

use App\EventCategory;
use App\EventTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventCategory = EventCategory::all();
        return view('eventcategory.index', compact('eventCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['eventCategories'] = EventCategory::all();
        return view('eventcategory.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventCategory::create($request->all());
        return redirect()->route('eventcategory.index')

            ->with('success', 'EventCategory created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\EventCategory $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['eventCategories'] = EventCategory::all();
        $where = array('id' => $id);
        $data['eventcategory_info'] = EventCategory::where($where)->first();
        return view('eventcategory.readingone')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['eventCategories'] = EventCategory::all();
        $where = array('id' => $id);
        $data['eventcategory_info'] = EventCategory::where($where)->first();
        return view('eventcategory.editingone')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
        ]);
        $category = EventCategory::find($id);
        $category->name = $request->get('name');
        $category->save();
        $eventCategory = EventCategory::all();
        return view("eventcategory.index",compact('eventCategory'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventCategory  $eventCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = EventCategory::find($id);
        $item -> delete();
        $eventCategory = EventCategory::all();

        return view("eventcategory.index",compact('eventCategory'));



    }
}
