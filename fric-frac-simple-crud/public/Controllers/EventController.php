<?php

namespace Fricfrac\Controllers;

class EventController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        $model['categoryOptions'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['topicOptions'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model);
    }
    public function readingOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $id);
        //var_dump($model['row']);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');

        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            "name" => $_POST['Events-Name'],
            "location" => $_POST['EventsLocation'],
            "starts" => $_POST['EventsStarts'],
            "ends" => $_POST['EventsEnd'],
            "image" => _POST['EventsImage'],
            "description" => $_POST['EventsDescription'],
            "organiserName" => $_POST['EventsOrganiserName'],
            "organiserDescription" => $_POST['EventsOrganiserDescription'],
            "eventCategoryID" => $_POST['Events-EventCategory'],
            "eventTopicID" => $_POST['Events-EventTopicId']

        );

        if (\AnOrmApart\Dal::create('Event', $event, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$event['Name']} is toegevoegd aan Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$event['Name']} niet toevoegen aan Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }
    //Deze is om de view juist te zien --> dus dat de gegevens zijn ingevuld
    public function updatingOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        $model['categoryOptions'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['topicOptions'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model);
    }
    public function updateOne()
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::update('Event', $event, 'Name')) {
            $model['message'] = "Rij aangepast! {$event['Name']} is aangepast in Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$event['Name']} niet aanpassen in Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }
    public function deleteOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        if (\AnOrmApart\Dal::delete('Event', $id)){
            $model['message'] = "Rij verwijdert!";
        } else{
            $model['message'] = "Rij niet verwijdert!";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }

}
