<?php

namespace Fricfrac\Controllers;

class EventTopicController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }
    public function readingOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $id);
        //var_dump($model['row']);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventTopic = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('EventTopic', $eventTopic, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$eventTopic['Name']} is toegevoegd aan EventTopic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventTopic['Name']} niet toevoegen aan EventTopic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }
    //Deze is om de view juist te zien --> dus dat de gegevens zijn ingevuld
    public function updatingOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }
    public function updateOne()
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventTopic = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::update('EventTopic', $eventTopic, 'Name')) {
            $model['message'] = "Rij aangepast! {$eventTopic['Name']} is aangepast in EventTopic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventTopic['Name']} niet aanpassen in EventTopic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }
    public function deleteOne($id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        if (\AnOrmApart\Dal::delete('EventTopic', $id)){
            $model['message'] = "Rij verwijdert!";
        } else{
            $model['message'] = "Rij niet verwijdert!";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }

}
