<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventTopic</h2>
            <a href="/EventTopic/UpdatingOne/<?php echo $model['row']['id'];?>" class="tile">
                <span class="icon-pencil"></span>
                <span class="screen-reader-text">Updating One</span>
            </a>
            <a href="/EventTopic/InsertingOne" class="tile">
                <span class="icon-plus"></span>
                <span class="screen-reader-text">Inserting One</span>
            </a>
            <a href="/EventTopic/deleteOne/<?php echo $model['row']['id'];?>" class="tile">
                <span class="icon-bin"></span>
                <span class="screen-reader-text">Delete One</span>
            </a>
            <a href="/EventTopic/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam</label>
                <span><?php echo $model['row']['name']; ?></span>
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>