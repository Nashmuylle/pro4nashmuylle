<section class="show-room entity">
    <form id="form" method="post" action="/EventTopic/updateOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventTopic</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <span class="icon-floppy-disk"></span>
                <span class="screen-reader-text">Update One</span>
            </button>
            <a href="/EventTopic/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <input type="hidden" id="Id" name="Id" value=<?php echo $model['row']['id']; ?> />
            <div>
                <label for="Name">Naam</label>
                <input id="Name" name="Name" type="text" value=<?php echo $model['row']['name']; ?> required />
            </div>
        </fieldset>
        <div class="feedback">
        </div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>