<section class="show-room entity">
    <form id="form" method="post" action="/Event/createOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <span class="icon-floppy-disk"></span>
                <span class="screen-reader-text">Insert One</span>
            </button>
            <a href="/Event/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>

            <label for="Events-Name">Naam</label>
            <input id="Events-Name" name="Events-Name" type="text" value="" required />
            </div>
            <div>
                <!-- HIER MOET ER EEN KEUZE KUNNEN WORDEN GEMAAKT-->
                <label for="Events-EventTopicId">EventTopic</label>
                <select name="Events-EventTopicId" id="Events-EventTopicId">
                    <?php
                            foreach ($model['topicOptions'] as $item){
                    ?>
                    <option value=<?php echo $item['id'];?>><?php echo $item['name'];?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div>
                <!-- HIER MOET ER EEN KEUZE KUNNEN WORDEN GEMAAKT-->
                <label for="Events-EventCategory">EventCategory</label>
                <select name="Events-EventCategory" id="Events-EventCategory">
                    <?php
                    foreach ($model['categoryOptions'] as $item){
                        ?>
                        <option value=<?php echo $item['id'];?>><?php echo $item['name'];?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div>
                <label for="EventsDescription">EventsDescription</label>
                <input id="EventsDescription" name="EventsDescription" type="text" value="" />
            </div>
            <div>
                <label for="EventsImage">EventsImage</label>
                <input id="EventsImage" name="EventsImage" type="text" value="" />
            </div>
            <div>
                <label for="EventsLocation">EventsLocation</label>
                <input id="EventsLocation" name="EventsLocation" type="text" value="" />
            </div>
            <div>
                <label for="EventsOrganiserDescription">OrganiserDescription</label>
                <input id="EventsOrganiserDescription" name="EventsOrganiserDescription" type="text" value="" />
            </div>
            <div>
                <label for="EventsOrganiserName">OrganiserName</label>
                <input id="EventsOrganiserName" name="EventsOrganiserName" type="text" value="" />
            </div>
            <div>
                <label for="EventsStarts">Starts</label>
                <input id="EventsStarts" name="EventsStarts" type="date" value="" />
            </div>
            <div>
                <label for="EventsEnd">Ends</label>
                <input id="EventsEnd" name="EventsEnd" type="date" value="" />
            </div>
        </fieldset>
        <div class="feedback">
            <p><?php echo $model['message']; ?></>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>