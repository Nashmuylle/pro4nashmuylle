<?php
/*
* JI
* 5/2/2020
* Leren werken met array's
*/

$persoon = array('Hans', 'Van Loveren', 'Goedestraat 20', '2000', 'Antwerpen');
echo $persoon[0];
echo ' woont in ' . $persoon[4];
// Binnen enkele min weet niet niemand meer dat stad op de vijfde
// positie in de array staat. Daarom werden associatieve array's ingevoerd.
// de index is geen getal maar een betekenisvolle tekst, bijv. city
$persoonAA = array('FirstName' =>'Hans', 
    'LastName' => 'Van Loveren', 
    'Street' => 'Goedestraat 20', 
    'PostalCode'=>'2000', 
    'City' => 'Antwerpen');
echo $persoonAA['LastName'] . ' woont in ' . $persoonAA['City'];

$personen = array($persoonAA, 
    array('FirstName' =>'Pieter', 
        'LastName' => 'Rubens', 
        'Street' => 'De Wapper 20', 
        'PostalCode'=>'2000', 
        'City' => 'Antwerpen')
);
echo $personen[0]['LastName'] . ' woont in ' . $personen[0]['City'];
echo $personen[1]['LastName'] . ' woont in ' . $personen[1]['City'];

foreach ($personen as $thisOne) {
    echo $thisOne['LastName'] . ' woont in ' . $thisOne['City'];
}

// Rubens verhuist
$personen[1]['Street'] = 'Goedehoopstraat 43';
echo $personen[1]['LastName'] . ' woont nu in ' . $personen[1]['Street'];
echo '<br/>';
$i = 0;
foreach ($personen as $thisOne) {
    echo ++$i . ' ' . $thisOne['LastName'] . ' woont in ' 
    . $thisOne['City'] . '<br/>';
}

$persoonAA = array('FirstName' =>'Herman', 
    'LastName' => 'Van Veen', 
    'Street' => 'Bromstraat 20', 
    'PostalCode'=>'2000', 
    'City' => 'Amsterdam');

    $personen[] = $persoonAA;
   $i = 0;
    foreach ($personen as $thisOne) {
        echo ++$i . ' ' . $thisOne['LastName'] . ' woont in ' 
        . $thisOne['City'] . '<br/>';
    }
 
    $sleutelBestaat = FALSE;
    $sleutelBestaat = array_key_exists ( 'sleutel' , $persoonAA );
    if ($sleutelBestaat) {
        echo '<br/>De sleutel met de naam sleutel bestaat'; 
    } else {
        echo '<br/>De sleutel met de naam sleutel bestaat niet';       
    }

    // vooraleer een nieuwe string-index toe te voegen
    // kan je verifieren als de sleutel al bestaat
    if (array_key_exists ( 'Leeftijd' , $persoonAA )) {
        echo '<br/>De sleutel met de naam Leeftijd bestaat al, kies een andere naam'; 
    } else {
        $persoonAA['Leeftijd'] = 20;
    }

    foreach ($persoonAA as $sleutel => $waarde) {
        echo '<br/> sleutel is ' . $sleutel . ' waarde is: ' . $waarde;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Werken met array's</title>
</head>
<a href="index.php">Terug naar index</a>
<body>
    
</body>
</html>