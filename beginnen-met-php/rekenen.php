<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rekenen</title>
</head>
<body>
    <p>
        <br></br>
        <br/>
        <br>
        <?php 
            define ('NEWLINE', '<br/><hr/>');
            $i = 0;
            echo $i++;
            echo NEWLINE;
            echo ++$i;
            echo NEWLINE;
            $getal = 100;
            $huisnummer = '100';
            echo ($getal == $huisnummer) ? 'waar' : 'onwaar';
            echo NEWLINE;
            echo ($getal === $huisnummer) ? 'waar' : 'onwaar';
            ?>
    </p>
</body>
</html>