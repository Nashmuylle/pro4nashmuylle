<?php
$voornaam = 'Sarah';
$familienaam = 'Costers';
$postcode = '2020';
$jaar = 2020;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Werken met tekst</title>
</head>

<body>
    <h1>
        <?php echo "je voornaam is {trim($voornaam)} en je familienaam is $familienaam"
        ?>
    </h1>
    <h2>
        <?php echo 'Je voornaam is ' . $voornaam . ' en je familienaam is '
            . $familienaam
        ?></h2>
    <p>
        <?php
        echo ($jaar === $postcode)
            ? "Het jaar $jaar is gelijk aan $postcode"
            : "Het jaar $jaar is niet gelijk aan $postcode";
        ?>
    </p>
    <p>
        <?php
        $price = 5;
        $tax = 12;
        echo sprintf('De maaltijd kost $%b', $price  +  ($price / 100 * $tax));
        ?>
    </p>
    <p>
        <?php 
        $zip = '6520';
        $month = 2;
        $day = 6;
        $year = 2018;
        echo sprintf("Postcode is %05d en de datum is %02d/%02d/%d", $zip, $month, $day, $year);
    ?>
    </p>
    <p>
        
    </p>
</body>

</html>