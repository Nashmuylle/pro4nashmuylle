<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
function array1_colorArray()
{
    $color = array('white', 'green', 'red', 'blue', 'black');
    echo "The memory of that scene for me is like a frame of film forever frozen at that moment: the $color[2] carpet, the $color[1] lawn, the $color[0] house, the leaden sky. The new president and his first lady. - Richard M. Nixon" . "\n";
}

function array2_colorsOrderdList()
{
    $color = array('white', 'green', 'red');
    foreach ($color as $c) {
        echo "$c, ";
    }
    sort($color);
    echo "<ul>";
    foreach ($color as $y) {
        echo "<li>$y</li>";
    }
    echo "</ul>";

}

function array3_capitals()
{

    $ceu = array("Italy" => "Rome", "Luxembourg" => "Luxembourg",
        "Belgium" => "Brussels", "Denmark" => "Copenhagen",
        "Finland" => "Helsinki", "France" => "Paris",
        "Slovakia" => "Bratislava", "Slovenia" => "Ljubljana",
        "Germany" => "Berlin", "Greece" => "Athens",
        "Ireland" => "Dublin", "Netherlands" => "Amsterdam",
        "Portugal" => "Lisbon", "Spain" => "Madrid",
        "Sweden" => "Stockholm", "United Kingdom" => "London",
        "Cyprus" => "Nicosia", "Lithuania" => "Vilnius",
        "Czech Republic" => "Prague", "Estonia" => "Tallin",
        "Hungary" => "Budapest", "Latvia" => "Riga", "Malta" => "Valetta",
        "Austria" => "Vienna", "Poland" => "Warsaw");
    asort($ceu);
    foreach ($ceu as $country => $capital) {
        echo "The capital of $country is $capital" . "\n";
    }

}

function array4_delete()
{

    $x = array(1, 2, 3, 4, 5);
    var_dump($x);
    unset($x[3]);
    $x = array_values($x);
    echo '
';
    var_dump($x);

}

function array5_firstElement()
{
    $color = array(4 => 'white', 6 => 'green', 11 => 'red');
    echo reset($color) . "\n";
}

function array6_readingJSON()
{

    function w3rfunction($value, $key)
    {
        echo "$key : $value" . "\n";
    }

    $a = '{"Title": "The Cuckoos Calling",
"Author": "Robert Galbraith",
"Detail":
{ 
"Publisher": "Little Brown"
 }
  }';
    $j1 = json_decode($a, true);
    array_walk_recursive($j1, "w3rfunction");

}

?>


</body>
</html>