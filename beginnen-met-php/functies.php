<?php
define('NEWLINE', '<br/><hr/>');
function teller(&$start = 100)
{
    for ($i = 1; $i <= 10; $i++) {
        echo $start++;
        echo NEWLINE;
    }
    return $i;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functies in PHP</title>
</head>

<body>
    <?php
    $getal = 1;
    echo 'Dit is de eerste keer: ';
    echo NEWLINE;
    teller($getal);
    echo 'Dit is de tweede keer: ';
    echo NEWLINE;
    echo teller($getal);
    
    $functieAlsWaarde = 'teller';
    echo "waarde van variabele functieAlsWaarde: $functieAlsWaarde";
    echo "de uitvoering van de waarde in functieAlsWaarde: ";
    $functieAlsWaarde($getal);

    echo 'Dit is de vierde keer: ';
    echo NEWLINE;
    echo teller();

    ?>
</body>

</html>